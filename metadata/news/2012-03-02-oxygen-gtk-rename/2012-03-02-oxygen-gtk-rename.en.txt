Title: x11-themes/oxygen-gtk has been renamed to x11-themes/oxygen-gtk{2,3}
Author: Timo Gurr <tgurr@exherbo.org>
Content-Type: text/plain
Posted: 2012-03-02
Revision: 1
News-Item-Format: 1.0
Display-If-Installed: x11-themes/oxygen-gtk

A GTK+3 version has been added and due to improper upsteam naming slotting
oxygen-gtk would probably cause problems in the future. Following other
distributions we now have two separate packages, oxygen-gtk{2,3}.

Please uninstall x11-themes/oxygen-gtk and install:

    * x11-themes/oxygen-gtk2 for GTK+2 theme support
    * x11-themes/oxygen-gtk3 for GTK+3 theme support
