# Copyright 2007 Alexander Færøy <ahf@exherbo.org>
# Copyright 2009, 2010, 2011, 2013 Ingmar Vanhassel <ingmar@exherbo.org>
# Copyright 2016 Bernd Steinhauser <berniyh@exherbo.org>
# Distributed under the terms of the GNU General Public License v2

require xorg [ suffix=tar.xz ]

export_exlib_phases pkg_setup

SUMMARY="Xorg video driver for AMD (only amdgpu kernel driver)"

LICENCES="X11"
SLOT="0"
MYOPTIONS="
    ( providers: eudev systemd ) [[ number-selected = exactly-one ]]
"

DEPENDENCIES="
    build:
        x11-proto/xorgproto
    build+run:
        x11-dri/libdrm[>=2.4.89][video_drivers:radeon(-)]
        x11-dri/mesa [[ note = [ Checks for libgbm ] ]]
        x11-server/xorg-server[>=1.16.0]
        providers:eudev? ( sys-apps/eudev )
        providers:systemd? ( sys-apps/systemd )
    suggestion:
        firmware/linux-firmware [[ description = [ contains firmware required for almost all ATI/AMD cards ] ]]
"

DEFAULT_SRC_CONFIGURE_PARAMS=(
    --enable-glamor
    --enable-udev
    --with-xorg-conf-dir=/usr/share/X11/xorg.conf.d
)

xf86-video-amdgpu_pkg_setup() {
    # Driver uses xf86LoadSubModule to load another xorg module at runtime which means that it
    # contains undefined symbols which would cause loading it to fail when built with -Wl,-z,now
    LDFLAGS+=" -Wl,-z,lazy"
}

