# Copyright 2007 Alexander Færøy <ahf@exherbo.org>
# Distributed under the terms of the GNU General Public License v2

require meson

SUMMARY="Fontconfig is a library for font customization and configuration"
HOMEPAGE="https://www.${PN}.org"
DOWNLOADS="${HOMEPAGE}/release/${PNV}.tar.gz"

LICENCES="MIT"
SLOT="0"
PLATFORMS="~amd64 ~arm ~armv7 ~armv8 ~x86"
MYOPTIONS="
    doc
"

DEPENDENCIES="
    build:
        dev-lang/python:*[>=3]
        dev-util/gperf
        sys-devel/gettext[>=0.19.7]
        virtual/pkg-config[>=0.9.0]
        doc? (
            app-text/docbook-sgml-dtd:3.1
            app-text/docbook-utils
        )
    build+run:
        dev-libs/expat
        media-libs/freetype:2[>=2.8.1]
    test:
        dev-libs/json-c:*
    recommendation:
        fonts/dejavu [[ description = [ Default fonts ] ]]
    suggestion:
        app-admin/eclectic-fontconfig[>=2.0.11] [[
            description = [ Manage fontconfig /etc/fonts/conf.d/ symlinks ]
        ]]
"

MESON_SRC_CONFIGURE_PARAMS=(
    # Don't run fc-cache, which we do manually below
    -Dcache-build=disabled
    -Ddefault-sub-pixel-rendering=rgb
    -Ddoc-pdf=disabled
    -Diconv=enabled
    -Dnls=enabled
    -Dtools=enabled
)
MESON_SRC_CONFIGURE_OPTION_FEATURES=(
    "doc doc"
    "doc doc-txt"
    "doc doc-man"
    "doc doc-html"
)

src_prepare() {
    meson_src_prepare

    # Disable testing with bind-mounted cache (violates sandbox)
    edo sed 's|BWRAP=`which bwrap`|BWRAP=""|' \
        -i test/run-test.sh

    # If meson only had a docdir param...
    edo sed -e "/install_dir: /s/'fontconfig'/'${PNVR}'/" -i doc/meson.build
}

src_install() {
    meson_src_install

    doman fc-*/fc-*.1 doc/fonts-conf.5
    dodoc doc/fontconfig-user.{txt,pdf}

    # fc-lang/ directory contains language coverage datafiles
    # which are needed to test the coverage of fonts.
    insinto /usr/share/fc-lang
    doins fc-lang/*.orth

    keepdir /var/cache/fontconfig
}

pkg_postinst() {
    if [[ ${ROOT} == "/" && -n $(echo "${ROOT}"/usr/share/fonts/**/**.ttf) ]]; then
        ebegin "Creating font cache"
            fc-cache --really-force --system-only
        eend $?
    fi
}

