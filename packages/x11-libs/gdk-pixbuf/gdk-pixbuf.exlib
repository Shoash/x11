# Copyright 2010 Saleem Abdulrasool <compnerd@compnerd.org>
# Copyright 2018 Rasmus Thomsen <cogitri@exherbo.org>
# Distributed under the terms of the GNU General Public License v2

require gnome.org [ suffix=tar.xz ]
require meson [ meson_minimum_version=0.55.3 cross_prefix=true ]
require gdk-pixbuf-loader

export_exlib_phases src_prepare pkg_prerm

SUMMARY="An image loading library"
HOMEPAGE="https://www.gnome.org/"

LICENCES="LGPL-2"
SLOT="2.0"
MYOPTIONS="
    gobject-introspection
    gtk-doc
    man-pages [[ description = [ Install man pages for gdk-pixbuf tools ] ]]
    tiff
    X [[ description = [ Install deprecated X11 integration API ] ]]

    ( providers: ijg-jpeg jpeg-turbo ) [[ number-selected = exactly-one ]]
"

DEPENDENCIES="
    build:
        sys-devel/gettext
        virtual/pkg-config
        gobject-introspection? ( gnome-desktop/gobject-introspection:1[>=0.9.3] )
        gtk-doc? ( dev-doc/gi-docgen[>=2021.2-r2] )
        man-pages? ( dev-python/docutils )
    build+run:
        dev-libs/glib:2[>=2.56.0]
        media-libs/libpng:=
        x11-misc/shared-mime-info
        providers:ijg-jpeg? ( media-libs/jpeg:= )
        providers:jpeg-turbo? ( media-libs/libjpeg-turbo )
        tiff? ( media-libs/tiff:= )
    post:
        X? ( x11-libs/gdk-pixbuf-xlib:2.0 )
"

MESON_SRC_CONFIGURE_PARAMS=(
    -Dbuiltin_loaders=none
    -Dgio_sniffing=true
    -Dinstalled_tests=false
    -Djpeg=enabled
    -Dpng=enabled
)

MESON_SRC_CONFIGURE_OPTION_FEATURES=(
    'gobject-introspection introspection'
    'tiff'
)

MESON_SRC_CONFIGURE_OPTION_SWITCHES=(
    'gtk-doc gtk_doc'
    'man-pages man'
)

MESON_SRC_CONFIGURE_TESTS=( '-Dtests=true -Dtests=false' )

GDK_PIXBUF_LOADERS_CACHE=/usr/$(exhost --target)/lib/gdk-pixbuf-2.0/2.10.0/loaders.cache

gdk-pixbuf_src_prepare(){
    meson_src_prepare
    edo ln -s "${WORKBASE}"/${PNV}/tests "${WORK}/"
}

gdk-pixbuf_pkg_prerm() {
    [[ -z ${REPLACED_BY_ID} ]] && edo rm -f "${ROOT}${GDK_PIXBUF_LOADERS_CACHE}"
}

