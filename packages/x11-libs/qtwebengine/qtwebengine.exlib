# Copyright 2015-2019 Heiko Becker <heirecka@exherbo.org>
# Distributed under the terms of the GNU General Public License v2

require qt [ docs=false ] qmake [ slot=5 ] ffmpeg

export_exlib_phases src_prepare src_configure src_compile

SUMMARY="Qt Cross-platform application framework: Integrate chromium into Qt"

LICENCES="
    FDL-1.3
    LGPL-3
"

MYOPTIONS="examples geolocation kerberos pulseaudio
    ( providers: ijg-jpeg jpeg-turbo ) [[ number-selected = exactly-one ]]
    ( providers: eudev systemd ) [[ number-selected = exactly-one ]]
    kerberos? ( ( providers: heimdal krb5 ) [[ number-selected = exactly-one ]] )
"

QT_MIN_VER="$(ever range -3)"

DEPENDENCIES="
    build:
        dev-lang/python:*[>=3]
        dev-lang/yasm
        dev-util/gperf
        sys-devel/bison
        sys-devel/flex
        sys-devel/ninja
        virtual/pkg-config
        x11-proto/xorgproto [[ note = glproto ]]
    build+run:
        app-arch/snappy
        app-spell/hunspell:=
        dev-lang/node[>=10]
        dev-libs/expat
        dev-libs/glib:2[>=2.32.0]
        dev-libs/icu:=[>=70]
        dev-libs/libevent:=
        dev-libs/libxml2:2.0
        dev-libs/libxslt
        dev-libs/nspr[>=4.0]
        dev-libs/nss[>=3.26]
        dev-libs/re2:=
        media-libs/fontconfig
        media-libs/freetype:2[>=2.4.2]
        media-libs/lcms2
        media-libs/libpng:=[>=1.6.0]
        media-libs/libvpx:=[>=1.6.0-r1]
        media-libs/libwebp:=[>=0.4]
        media-libs/opus[>=1.3.1]
        sys-apps/dbus
        sys-apps/pciutils
        sys-libs/zlib
        sys-sound/alsa-lib[>=1.0.10]
        x11-dri/libdrm
        x11-dri/mesa
        x11-libs/harfbuzz[>=2.4.0]
        x11-libs/libX11
        x11-libs/libXcomposite
        x11-libs/libXcursor
        x11-libs/libXdamage
        x11-libs/libXext
        x11-libs/libXfixes
        x11-libs/libXi
        x11-libs/libxkbcommon
        x11-libs/libxkbfile
        x11-libs/libXrandr
        x11-libs/libXrender
        x11-libs/libXScrnSaver
        x11-libs/libXtst
        x11-libs/libxcb
        x11-libs/qtbase:${SLOT}[>=${QT_MIN_VER}][gui]
        x11-libs/qtdeclarative:${SLOT}[>=${QT_MIN_VER}]
        x11-libs/qtquickcontrols2:${SLOT}[>=${QT_MIN_VER}]
        x11-libs/qttools:${SLOT}[>=${QT_MIN_VER}]   [[ note = [ QtDesigner ] ]]
        x11-libs/qtwebchannel:${SLOT}[>=${QT_MIN_VER}]
        x11-libs/qtxmlpatterns:${SLOT}[>=${QT_MIN_VER}]
        examples? ( x11-libs/qtquickcontrols:${SLOT}[>=${QT_MIN_VER}] )
        geolocation? ( x11-libs/qtlocation:${SLOT}[>=${QT_MIN_VER}] )
        kerberos? (
            providers:heimdal? ( app-crypt/heimdal )
            providers:krb5? ( app-crypt/krb5 )
        )
        providers:eudev? ( sys-apps/eudev )
        providers:ijg-jpeg? ( media-libs/jpeg:= )
        providers:jpeg-turbo? ( media-libs/libjpeg-turbo )
        providers:systemd? ( sys-apps/systemd )
        pulseaudio? ( media-sound/pulseaudio[>=0.9.10] )
"

# 83e790dae14725347180bb79e463c5f242616b8d in qtbase.git (part of v5.9.2):
# "We currently don't support unbundling SRTP because Chromium uses a too
# new unreleased version, but we were still testing for it and claiming to
# use the system one if found."

qtwebengine_src_prepare() {
    default
    expatch "${FILES}"/chromium-remove-no-canonical-prefixes.patch

    # Since qtwebengine bundles chromium we need to use the same workaround,
    # add appropriate symlinks and add them to the PATH...
    local dir=${WORKBASE}/symlinked-build-tools
    edo mkdir -p ${dir}
    edo ln -s /usr/$(exhost --target)/bin/$(exhost --target)-pkg-config ${dir}/pkg-config
    edo ln -s /usr/$(exhost --target)/bin/$(exhost --target)-ar ${dir}/ar
    edo ln -s /usr/$(exhost --target)/bin/$(exhost --target)-gcc ${dir}/gcc
    edo ln -s /usr/$(exhost --target)/bin/$(exhost --target)-g++ ${dir}/g++
    edo ln -s /usr/$(exhost --target)/bin/$(exhost --target)-nm ${dir}/nm
    edo ln -s /usr/$(exhost --target)/bin/$(exhost --target)-readelf ${dir}/readelf
    export PATH="${dir}:${PATH}"

    edo sed -e "/check_call/ s/strip/$(exhost --build)-strip/" \
            -i src/3rdparty/chromium/tools/gn/bootstrap/bootstrap.py

    # *Sigh* - Avoid including already installed headers from qtwebengine
    # https://bugreports.qt.io/browse/QTBUG-72601
    edo find "${WORK}" -type f -name "*.pr[fio]" | xargs sed -i -e 's|INCLUDEPATH += |&$$QTWEBENGINE_ROOT/include |'

    if [[ $(exhost --target) == *-musl* ]] ; then
        edo sed -e "s/#if __GLIBC__ < 2 || __GLIBC_MINOR__ < 17/#if false/" \
            -i src/buildtools/configure.json

        edo cp "${FILES}"/resolv_compat.h \
            "${WORK}"/src/3rdparty/chromium/net/dns

        expatch "${FILES}"/musl-canonicalize-file-name-musl.patch
        expatch "${FILES}"/musl-stackstart.patch
        expatch "${FILES}"/musl-resolve.patch
        expatch "${FILES}"/musl-msgvec-push_back.patch
        expatch "${FILES}"/musl-cstring.patch
        expatch "${FILES}"/musl-execinfo.patch
        expatch "${FILES}"/musl-no-mallinfo.patch
        expatch "${FILES}"/musl-replace-pvalloc.patch
        expatch "${FILES}"/musl-thread-stacksize.patch
        expatch "${FILES}"/musl-chromium-default-stacksize.patch
    fi
}

qtwebengine_src_configure() {
    export NINJA_PATH=/usr/$(exhost --target)/bin/ninja

    if option examples ; then
        EQMAKE_PARAMS+=( QT_BUILD_PARTS+=examples )
    else
        EQMAKE_PARAMS+=( QT_BUILD_PARTS-=examples )
    fi

    local qmake_params=()

    qmake_params+=(
        -build-qtpdf
        # Provides PipeWire support in WebRTC using GIO, fails to find the pipewire header
        -no-feature-webengine-webrtc-pipewire
        -no-feature-webengine-sanitizer
        -no-webengine-embedded-build
        -pdf-widgets
        #--feature-pdf-v8
        #--feature-pdf-xfa
        #--feature-pdf-xfa-bmp
        #--feature-pdf-xfa-gif
        #--feature-pdf-xfa-png
        -system-webengine-icu
        -system-webengine-ffmpeg
        -system-webengine-opus
        -system-webengine-webp
        -webengine-alsa
        -webengine-extensions
        -webengine-pepper-plugins
        -webengine-printing-and-pdf
        -webengine-proprietary-codecs
        -webengine-qml
        -webengine-spellchecker
        -webengine-webchannel
        -webengine-webrtc
        -webengine-widgets
        $(qt_enable geolocation webengine-geolocation)
        $(qt_enable kerberos webengine-kerberos)
        $(qt_enable pulseaudio webengine-pulseaudio)
    )

    eqmake -- "${qmake_params[@]}"
}

qtwebengine_src_compile() {
    export NINJAFLAGS="-v -j${EXJOBS:-1}"

    # Seems qtwebengine gets confused by our AR env variable, perhaps we can set
    # ar_cmd in src/3rdparty/chromium/third_party/libvpx/unpack_lib_posix.gypi instead?
    unset AR

    qt_src_compile
}

