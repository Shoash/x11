# Copyright 2023 Heiko Becker <heirecka@exherbo.org>
# Distributed under the terms of the GNU General Public License v2

require qt cmake [ ninja=true ]

export_exlib_phases src_compile src_install

SUMMARY="Qt Cross-platform application framework: QtLocation"
DESCRIPTION="
The Qt Location module helps you create mapping solutions using data available
from popular location service providers, such as Open Street Map.
The Qt Location API enables you to:
* Access and present map data.
* Support touch gesture on a specific area of the map.
* Query for a specific geographical location and route.
* Add additional layers on top, such as polylines and circles.
* Search for places and related images."

LICENCES="FDL-1.3 GPL-2 GPL-3 LGPL-3"
MYOPTIONS="
    examples
"

DEPENDENCIES="
    build+run:
        x11-libs/qtbase:${SLOT}[>=${PV}]
        x11-libs/qtdeclarative:${SLOT}[>=${PV}]
        x11-libs/qtpositioning:${SLOT}[>=${PV}][qml]
        x11-libs/qtshadertools:${SLOT}[>=${PV}]
"

CMAKE_SRC_CONFIGURE_OPTIONS=(
    'examples QT_BUILD_EXAMPLES'
)

qtlocation-6_src_compile() {
    cmake_src_compile

    option doc && eninja docs
}

qtlocation-6_src_install() {
    cmake_src_install

    option doc && DESTDIR="${IMAGE}" eninja install_docs
}

