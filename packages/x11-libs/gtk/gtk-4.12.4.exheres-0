# Copyright 2020 Marc-Antoine Perennou <keruspe@exherbo.org>
# Copyright 2008 Saleem Abdulrasool <compnerd@compnerd.org>
# Distributed under the terms of the GNU General Public License v2
# Based in part upon 'gtk+-2.13.2.ebuild' from Gentoo which is:
#    Copyright 1999-2008 Gentoo Foundation

require gtk
require option-renames [ renames=[ 'gstreamer media' ] ]

SLOT="4.0"
PLATFORMS="~amd64 ~armv8 ~x86"

MYOPTIONS="
    colord [[
        description = [ Color profiling support for the CUPS printing backend ]
        requires = [ cups ]
    ]]
    cups
    gtk-doc
    media [[
        description = [ Support audio/video playback ]
        presumed = true
    ]]
    tracker [[ description = [ Search engine based on tracker ] ]]
    wayland
    X
    ( wayland X ) [[ number-selected = at-least-one ]]

    ( providers: ijg-jpeg jpeg-turbo ) [[ number-selected = exactly-one ]]
"

DEPENDENCIES="
    build:
        dev-doc/gi-docgen[>=2021.1]
        dev-python/docutils
        sys-devel/gettext[>=0.19.7]
        virtual/pkg-config[>=0.20]
        X? ( x11-proto/xorgproto )
        wayland? ( sys-libs/wayland-protocols[>=1.32] )
    build+run:
        dev-libs/fribidi[>=1.0.6]
        dev-libs/glib:2[>=2.72.0]
        dev-libs/libepoxy[>=1.4][X?]
        gnome-desktop/gobject-introspection:1[>=1.72.0]
        gnome-desktop/librsvg:2[>=2.52.0] [[ note = [ Automagic dependency of gtk-demo ] ]]
        media-libs/fontconfig
        media-libs/libpng:=
        media-libs/tiff:=[>=4]
        x11-dri/mesa[X?][wayland?]
        x11-libs/cairo[>=1.14.0][X?]
        x11-libs/gdk-pixbuf:2.0[>=2.30.0][gobject-introspection]
        x11-libs/graphene:1.0[>=1.10.2][gobject-introspection]
        x11-libs/harfbuzz[>=2.6.0]
        x11-libs/libxkbcommon[>=0.2.0][X?][wayland?]
        x11-libs/pango[>=1.50.0][gobject-introspection]
        colord? ( sys-apps/colord[>=0.1.9] )
        cups? ( net-print/cups[>=1.2] )
        media? (
            media-libs/gstreamer:1.0[>=1.12.3]
            media-plugins/gst-plugins-bad:1.0[>=1.12.3] [[ note = [ gstreamer-player ] ]]
            media-plugins/gst-plugins-base:1.0[>=1.12.3][X?][wayland?][gstreamer_plugins:opengl]
        )
        tracker? ( app-pim/tracker:3.0 )
        wayland? ( sys-libs/wayland[>=1.21.0] )
        X? (
            x11-libs/libX11
            x11-libs/libXext
            x11-libs/libXinerama
            x11-libs/libXi
            x11-libs/libXrandr[>=1.5]
            x11-libs/libXcursor
            x11-libs/libXfixes
            x11-libs/libXdamage
        )
        providers:ijg-jpeg? ( media-libs/jpeg:= )
        providers:jpeg-turbo? ( media-libs/libjpeg-turbo )
        !x11-libs/gtk+:4.0 [[
            description = [ Package renamed ]
            resolution = uninstall-blocked-before
        ]]
    run:
        media? (
            media-plugins/gst-plugins-base:1.0[gstreamer_plugins:opus][gstreamer_plugins:vorbis] [[
                note = [ audio codec support for WebM ]
            ]]
            virtual/gst-plugin-vpx:1.0 [[ note = [ video codec support for WebM ] ]]
        )
    post:
        x11-themes/hicolor-icon-theme
    recommendation:
        gnome-desktop/adwaita-icon-theme [[
            description = [ Many apps use icons from this theme ]
        ]]
    suggestion:
        app-vim/gtk-syntax [[
            description = [ A collection of vim syntax files for various GTK+ C extensions ]
        ]]
        gnome-desktop/evince [[
            description = [ Used for print preview functionality ]
        ]]
        cups? (
            net-dns/avahi [[
                description = [ Used for mDNS printer discovery support ]
            ]]
        )
"

# Require X
RESTRICT="test"

MESON_SRC_CONFIGURE_PARAMS+=(
    '-Dbroadway-backend=false'
    '-Dwin32-backend=false'
    '-Dmacos-backend=false'

    # ffmpeg is experimental, disabled by default and may even be removed
    # https://gitlab.gnome.org/GNOME/gtk/-/issues/5581
    '-Dmedia-ffmpeg=disabled'

    # TODO: we should provide this as an alternative to cups
    '-Dprint-cpdb=disabled'

    # vulkan renderer is experimental, and may have rendering/performance issues
    '-Dvulkan=disabled'
    '-Dcloudproviders=disabled'
    '-Dsysprof=disabled'

    '-Dman-pages=true'
    '-Dintrospection=enabled'

    '-Dbuild-demos=true'
    '-Dbuild-testsuite=false'
)
MESON_SRC_CONFIGURE_OPTION_FEATURES=(
    'media media-gstreamer'

    'cups print-cups'

    colord
    tracker
)
MESON_SRC_CONFIGURE_OPTION_SWITCHES=(
    'X x11-backend'
    'wayland wayland-backend'

    'gtk-doc documentation'
)
MESON_SRC_CONFIGURE_TESTS=(
    '-Dbuild-tests=true -Dbuild-tests=false'
)

src_install() {
    meson_src_install

    gtk_alternatives

    edo find "${IMAGE}" -type d -empty -delete
}

