# Copyright 2018 Rasmus Thomsen <cogitri@exherbo.org>
# Based in part upon 'mesa.exlib' which is:
# Copyright 2008 Alexander Færøy <eroyf@eroyf.org>
# Copyright 2008-2011 Ingmar Vanhassel <ingmar@exherbo.org>
# Copyright 2013 Saleem Abdulrasool <compnerd@compnerd.org>

if ever is_scm ; then
    SCM_REPOSITORY="https://gitlab.freedesktop.org/${PN}/${PN}.git"
    SCM_BRANCH="main"
    require scm-git
else
    DOWNLOADS="https://mesa.freedesktop.org/archive/${PNV}.tar.xz"
    UPSTREAM_RELEASE_NOTES="https://www.mesa3d.org/relnotes/${PV}.html"
fi

# Python and Mako are just build deps, so multibuild doesn't make much sense.
require python [ blacklist=2 multibuild=false ]
require meson
require option-renames [ renames=[ 'va vaapi' ] ]

export_exlib_phases src_prepare src_configure src_install pkg_postinst

SUMMARY="Mesa's OpenGL library"
HOMEPAGE="https://www.mesa3d.org"

LICENCES="MIT"
SLOT="0"

MYOPTIONS="
    d3d9 [[
        description = [ Gallium Direct3D 9.x state tracker ]
        requires = [ llvm X ]
    ]]
    d3d9? (
        video_drivers:
            (
                crocus
                i915
                iris
                nouveau
                r300
                radeon
                vmware
            ) [[ number-selected = at-least-one ]]
    )
    debug
    llvm [[ description = [ Using LLVM as the shader backend for Gallium ] ]]
    rusticl [[
        description = [ Build the rusticl OpenCL driver ]
        requires = [ llvm ]
    ]]
    rusticl? (
        ( video_drivers: gallium-swrast i915 nouveau r300 radeon ) [[ number-selected = at-least-one ]]
    )
    sensors [[ description = [ Gallium extra HUD and sensors support ] ]]
    sensors? ( ( video_drivers: nouveau radeon ) [[ number-selected = at-least-one ]] )
    vaapi [[ description = [ Enable the VA-API state tracker ] ]]
    vaapi? (
        ( X wayland ) [[ number-selected = at-least-one ]]
        video_drivers:
            (
                d3d12
                nouveau
                radeon
            ) [[ number-selected = at-least-one ]]
    )
    vdpau [[
        description = [ Enable support for VDPAU hardware accelerated video decoding ]
        requires = [ X ]
    ]]
    vulkan-overlay [[ description = [ A Vulkan layer to display information using an overlay ] ]]
    vulkan-overlay? (
        video_drivers:
            (
                crocus
                iris
                radeon
            ) [[ number-selected = at-least-one ]]
    )
    wayland
    X [[ description = [ Enable GLX ] presumed = true ]]
    xa [[ description = [ Enable the X Acceleration API (xatracker) ] ]]
    zstd [[ description = [ Use the ZStandard compression algorithm for shader cache if possible ] ]]
    (
        platform:
            amd64
            x86
        video_drivers:
            d3d12 [[ description = [ Gallium driver for Microsoft D3D12 API ] ]]
            crocus [[ description = [ Gallium driver for Intel i965 to Haswell (Gen4 to Gen7) GPUs ] ]]
            i915 [[ description = [ Gallium driver for Intel i915 (Gen3) GPUs ] ]]
            iris [[ description = [ Gallium driver for Intel Broadwell/Braswell (Gen8) and newer GPUs ] ]]
            gallium-swrast [[
                description = [ Gallium based software driver ]
                requires = [ llvm ]
            ]]
            nouveau [[ description = [ (Gallium) Generally for newer Nvidia cards ] ]]
            (
                r300 [[ description = [ (Gallium) Driver for Radeon cards from 9500 (r300) to X1950 (r500) ] ]]
                radeon [[ description = [ (Gallium) Driver for the Radeon HD-2000 (r600) and newer ] ]]
            ) [[ requires = [ llvm video_drivers: gallium-swrast ] ]]
            virtio-gpu [[ description = [ Gallium driver for guest systems on hypervisors with VirtIO-GPU ] ]]
            vmware [[ description = [ Gallium driver for VMware's SVGA virtual GPU ] ]]
    )
    (
        platform:
            armv7
        video_drivers:
            (
                lima [[ description = [ Gallium driver for Mali 4xx GPUs ] ]]
                panfrost [[ description = [ Gallium driver for Mali Midgard (Txxx) and Bifrost (Gxx) GPUs ] ]]
                vc4 [[ description = [ Gallium driver for Broadcom's VideoCore 4 GPU ] ]]
            )
    )
    llvm? (
        ( video_drivers: gallium-swrast i915 nouveau r300 radeon ) [[ number-selected = at-least-one ]]
    )
    valgrind [[ description = [ Support valgrind intrinsics to suppress false warnings ] ]]
    vdpau? ( ( video_drivers: d3d12 nouveau r300 radeon virtio-gpu ) [[ number-selected = at-least-one ]] )
    xa? ( ( video_drivers: i915 nouveau vmware ) [[ number-selected = at-least-one ]] )

    video_drivers:
        (
            crocus
            d3d12
            gallium-swrast
            i915
            iris
            lima
            nouveau
            panfrost
            r300
            radeon
            vc4
            virtio-gpu
            vmware
        ) [[ number-selected = at-least-one ]]

    ( providers: eudev systemd ) [[ number-selected = exactly-one ]]
"

# NOTE(Cogitri): The below version is needed to build mesa at all
LIBDRM_REQUIRED='[>=2.4.109]'
# The below version is needed to build amdvk or radeonsi
LIBDRM_REQUIRED_AMDGPU='[>=2.4.110]'
# The below version is needed to build any of the other radeon drivers
LIBDRM_REQUIRED_RADEON='[>=2.4.71]'
# The below version is needed to build dri or gallium nouveau
LIBDRM_REQUIRED_NOUVEAU='[>=2.4.102]'
# The below version is needed to build dri or gallium i915
LIBDRM_REQUIRED_INTEL='[>=2.4.75]'
# NOTE(Cogitri): for xcb-glx
XCBDRI2_REQUIRED='[>=1.8.1]'

# TODO state trackers: OpenMAX(omx), requires additional new dependency libomxil-bellagio
# NOTE: For more information about the need for the intel-compute-runtime blocker:
# https://bugs.llvm.org/show_bug.cgi?id=30587
# https://bugs.debian.org/cgi-bin/bugreport.cgi?bug=852746
DEPENDENCIES="
    build:
        dev-libs/libpthread-stubs[>=0.4]
        dev-python/Mako[>=0.8.0][python_abis:*(-)?]
        sys-devel/bison
        sys-devel/flex
        sys-devel/meson[python_abis:*(-)?]
        virtual/pkg-config[>=0.9.0]
        !x11-dri/eclectic-opengl
        rusticl? (
            dev-rust/bindgen-cli[>=0.62.0]
            dev-libs/libclc:=
        )
        video_drivers:crocus? ( dev-lang/glslang )
        video_drivers:d3d12? ( x11-libs/directx-headers[>=1.610.0] )
        video_drivers:iris? ( dev-lang/glslang )
        video_drivers:radeon? ( dev-lang/glslang )
        vulkan-overlay? ( dev-lang/glslang )
        X? ( x11-proto/xorgproto )
    build+run:
        dev-libs/expat
        sys-libs/zlib[>=1.2.3]
        x11-dri/libdrm${LIBDRM_REQUIRED}
        llvm? (
            dev-lang/llvm:=[>=5.0.0][-static(-)] [[
                note = [ meson build system doesn't support static llvm ]
            ]]
        )
        sensors? ( sys-apps/lm_sensors[>=3.4.0] )
        providers:eudev? ( sys-apps/eudev )
        providers:systemd? ( sys-apps/systemd )
        rusticl? ( dev-lang/rust:=[>=1.60] )
        vaapi? ( x11-libs/libva[>=2.8.0] )
        valgrind? ( dev-util/valgrind )
        vdpau? ( x11-libs/libvdpau[>=1.1] )
        video_drivers:i915? ( x11-dri/libdrm${LIBDRM_REQUIRED_INTEL}[video_drivers:intel(+)] )
        video_drivers:nouveau? ( x11-dri/libdrm${LIBDRM_REQUIRED_NOUVEAU}[video_drivers:nouveau(-)] )
        video_drivers:r300? ( x11-dri/libdrm${LIBDRM_REQUIRED_RADEON}[video_drivers:radeon(-)] )
        video_drivers:radeon? (
            dev-lang/llvm:=[>=15.0.0][-static(-)]
            dev-util/elfutils
            x11-dri/libdrm${LIBDRM_REQUIRED_AMDGPU}[video_drivers:radeon(-)]
        )
        video_drivers:vc4? ( x11-dri/libdrm[video_drivers:vc4(-)] )
        video_drivers:vmware? ( x11-dri/libdrm[video_drivers:vmware(-)] )
        wayland? (
            sys-libs/wayland[>=1.18]
            sys-libs/wayland-protocols[>=1.24]
        )
        X? (
            x11-apps/xrandr[>=1.3] [[ *note = [ for xlib-lease ] ]]
            x11-libs/libICE
            x11-libs/libX11[xcb(+)]
            x11-libs/libXau
            x11-libs/libxcb${XCBDRI2_REQUIRED}
            x11-libs/libXdmcp
            x11-libs/libXext
            x11-libs/libXfixes[>=2.0]
            x11-libs/libxshmfence[>=1.1]
            x11-libs/libXxf86vm
            x11-utils/xcb-util-keysyms
        )
        zstd? ( app-arch/zstd )
        !sys-libs/wayland[<1.15.0] [[
            description = [ wayland imported libwayland-egl from mesa ]
            resolution = uninstall-blocked-after
        ]]
        dev-libs/libglvnd[>=1.3.2][X=]
    run:
        !media-libs/libtxc_dxtn [[
                description = [ mesa now bundles libtxc_dxtn ]
                resolution = uninstall-blocked-after
        ]]
    suggestion:
        x11-dri/mesa-demos [[
            description = [ Provides useful programs like glxinfo ]
        ]]
"

if ever at_least 23.3.0; then
    DEPENDENCIES+="
        build:
            rusticl? ( sys-devel/meson[>=1.2.0][python_abis:*(-)?] )
        build+run:
            wayland? ( sys-libs/wayland-protocols[>=1.30] )
    "
fi

mesa_src_prepare() {
    # Mesa's meson build system requires pkg-config to do some
    # fancy, custom stuff, which use pkg-config directly.
    edo mkdir "${WORKBASE}"/bin
    edo ln -s "/usr/$(exhost --build)/bin/${PKG_CONFIG}" "${WORKBASE}"/bin/pkg-config
    export PATH="${WORKBASE}/bin:${PATH}"

    meson_src_prepare

    # Respect selected python abi
    edo sed \
        -e "s:python3:python$(python_get_abi):g" \
        -i meson.build
}

mesa_src_configure() {
    EGL_PLATFORMS=( )
    GALLIUM_DRIVERS=( )
    VULKAN_DRIVERS=( )
    VULKAN_LAYERS=( )

    # TODO: freedreno,softpipe,etnaviv,svga,zink,virtio-experimental,asahi
    option video_drivers:d3d12              && GALLIUM_DRIVERS+=( d3d12 )
    option video_drivers:gallium-swrast     && GALLIUM_DRIVERS+=( swrast )
    option video_drivers:i915               && GALLIUM_DRIVERS+=( i915 )
    option video_drivers:crocus             && GALLIUM_DRIVERS+=( crocus )
    option video_drivers:iris               && GALLIUM_DRIVERS+=( iris )
    option video_drivers:lima               && GALLIUM_DRIVERS+=( lima )
    option video_drivers:nouveau            && GALLIUM_DRIVERS+=( nouveau )
    option video_drivers:panfrost           && GALLIUM_DRIVERS+=( panfrost )
    option video_drivers:r300               && GALLIUM_DRIVERS+=( r300 )
    option video_drivers:radeon             && GALLIUM_DRIVERS+=( r600 radeonsi )
    option video_drivers:vc4                && GALLIUM_DRIVERS+=( vc4 )
    option video_drivers:virtio-gpu         && GALLIUM_DRIVERS+=( virgl )
    option video_drivers:vmware             && GALLIUM_DRIVERS+=( svga )

    option X                                && EGL_PLATFORMS+=( x11 )
    option wayland                          && EGL_PLATFORMS+=( wayland )

    option video_drivers:gallium-swrast     && VULKAN_DRIVERS+=( swrast )
    # Enable the intel vulkan drivers if any driver supporting Gen7 or later is selected
    option video_drivers:crocus || \
    option video_drivers:iris               && VULKAN_DRIVERS+=( intel intel_hasvk )
    option video_drivers:radeon             && VULKAN_DRIVERS+=( amd )
    option video_drivers:virtio-gpu         && VULKAN_DRIVERS+=( virtio )

    option vulkan-overlay                   && VULKAN_LAYERS+=( overlay )

    local MESA_SRC_CONFIGURE_PARAMS

    MESA_SRC_CONFIGURE_PARAMS+=(
        --buildtype=$(option debug debugoptimized release)
        -Dallow-kcmp=enabled
        -Dandroid-libbacktrace=disabled
        -Dandroid-stub=false
        -Dbuild-aco-tests=false
        -Ddri3=enabled
        -Degl=enabled
        -Degl-native-platform=auto
        -Denable-glcpp-tests=false
        -Dexpat=enabled
        -Dfreedreno-kmds=msm
        -Dgallium-d3d10umd=false
        -Dgallium-drivers=$(IFS=, ; echo "${GALLIUM_DRIVERS[*]}")
        -Dgallium-omx=disabled
        -Dgallium-opencl=disabled
        -Dgallium-windows-dll-name=libgallium_wgl
        -Dgbm=enabled
        -Dgles1=enabled
        -Dgles2=enabled
        -Dglvnd=true
        -Dglvnd-vendor-name=mesa
        -Dglx-direct=true
        -Dglx-read-only-text=false
        -Dimagination-srv=false
        -Dinstall-intel-gpu-tests=false
        -Dintel-clc=disabled
        -Dintel-xe-kmd=disabled
        -Dlibunwind=disabled
        -Dmicrosoft-clc=disabled
        -Dmin-windows-version=8
        -Dopencl-external-clang-headers=enabled
        -Dopencl-spirv=false
        -Dopengl=true
        -Dperfetto=false
        -Dplatforms=$(IFS=, ; echo "${EGL_PLATFORMS[*]}")
        -Dpower8=disabled
        -Dradv-build-id=''
        -Dshader-cache=enabled
        -Dshader-cache-default=true
        -Dshader-cache-max-size=1GB
        -Dshared-glapi=enabled
        -Dshared-llvm=enabled
        -Dspirv-to-dxil=false
        -Dsse2=true
        -Dvideo-codecs=h264dec,h264enc,h265dec,h265enc,vc1dec
        -Dvmware-mks-stats=false
        -Dvulkan-beta=false
        -Dvulkan-drivers=$(IFS=, ; echo "${VULKAN_DRIVERS[*]}")
        -Dvulkan-layers=$(IFS=, ; echo "${VULKAN_LAYERS[*]}")
        -Dxmlconfig=enabled
        -Dzlib=enabled
        $(meson_feature llvm)
        $(meson_feature sensors lmsensors)
        $(meson_feature vaapi gallium-va)
        $(meson_feature valgrind)
        $(meson_feature vdpau gallium-vdpau)
        $(meson_feature video_drivers:d3d12 gallium-d3d12-video)
        $(meson_feature X xlib-lease)
        $(meson_feature xa gallium-xa)
        $(meson_feature zstd)
        $(meson_switch d3d9 gallium-nine)
        $(meson_switch !debug b_ndebug)
        $(meson_switch llvm draw-use-llvm)
        $(meson_switch rusticl gallium-rusticl)
        $(meson_switch sensors gallium-extra-hud)
        $(meson_switch video_drivers:gallium-swrast osmesa)
        $(meson_switch X glx dri disabled)
        $(option rusticl -Drust_std=2021 '')
    )

    if ever at_least 23.3.0; then
        MESA_SRC_CONFIGURE_PARAMS+=(
            -Dgpuvis=false
        )
    fi

    exmeson "${MESA_SRC_CONFIGURE_PARAMS[@]}"
}

mesa_src_install() {
    dodir /usr

    meson_src_install

    # Cause collisions with glew
    edo rm -f "${IMAGE}"/usr/$(exhost --target)/include/GL/{glew,glxew,wglew}.h

    edo rm -f "${IMAGE}"/usr/$(exhost --target)/lib/libGLESv{1,2}*
    edo rm -f "${IMAGE}"/usr/$(exhost --target)/lib/pkgconfig/glesv{1_cm,2}.pc
}

mesa_pkg_postinst() {
    # Ensure that libglvnd is selected after the alternative got removed from mesa
    # This does however mean that there won't be a libglx.so xserver module until xorg-server is
    # rebuilt, which moves that file to the glvnd alternative
    # --force is needed because there might be collisions with the bundled libglvnd in nvidia-driver
    # which go away once that is updated
    # added 2019-01-27, remove after a transitionary period
    if [[ $(eclectic opengl show) == X.org ]]; then
        edo eclectic opengl set --force glvnd
    fi
}

