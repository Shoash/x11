# Copyright 2021 Marc-Antoine Perennou <keruspe@exherbo.org>
# Distributed under the terms of the GNU General Public License v2

require xorg meson

SUMMARY="The X server for wayland"
DOWNLOADS="https://www.x.org/releases/individual/xserver/${PNV}.tar.xz"

LICENCES="X11"
SLOT="0"
PLATFORMS="~amd64"
MYOPTIONS="
    eglstream [[ description = [ xwayland eglstream support (proprietary Nvidia driver) ] ]]
    systemd
    ( providers: libressl openssl ) [[ number-selected = exactly-one ]]
"

# FIXME
RESTRICT="test"

DEPENDENCIES="
    build:
        x11-proto/xorgproto[>=2021.5]
    build+run:
        dev-libs/libbsd [[ note = [ automagic ] ]]
        dev-libs/libepoxy[>=1.5.4]
        dev-libs/libglvnd [[ note = [ provides libGL ] ]]
        sys-libs/wayland[>=1.21.0]
        sys-libs/wayland-protocols[>=1.30]
        x11-apps/xkbcomp
        x11-dri/libdrm[>=2.4.109]
        x11-dri/mesa[>=21.1.0]
        x11-libs/libX11[>=1.6]
        x11-libs/libXau
        x11-libs/libxcvt
        x11-libs/libXdmcp
        x11-libs/libXext[>=1.0.99.4]
        x11-libs/libXfixes
        x11-libs/libXfont2[>=2.0.0]
        x11-libs/libXi[>=1.2.99.1]
        x11-libs/libxkbfile
        x11-libs/libXmu
        x11-libs/libXpm
        x11-libs/libXrender
        x11-libs/libXres
        x11-libs/libxshmfence[>=1.1]
        x11-libs/libXv[>=1.0.5]
        x11-libs/pixman:1[>=0.27.2]
        x11-libs/xtrans[>=1.3.5]
        x11-server/xorg-server[>=1.20.10-r1]
        eglstream? ( sys-libs/egl-wayland[>=1.0.2] )
        systemd? ( sys-apps/systemd[>=209] [[ note = [ automagic libsystemd ] ]] )
        providers:libressl? ( dev-libs/libressl:= )
        providers:openssl? ( dev-libs/openssl:= )
"

# FIXME: enable secure-rpc
MESON_SRC_CONFIGURE_PARAMS=(
    -Ddefault_font_path="/usr/share/fonts/X11"
    -Ddevel-docs=false
    -Ddocs=false
    -Ddocs-pdf=false
    -Ddpms=true
    -Ddri3=true
    -Ddrm=true
    -Dglamor=true
    -Dglx=true
    -Dinput_thread=true
    -Dipv6=true
    -Dlibdecor=false
    -Dlibunwind=false
    -Dlisten_local=true
    -Dlisten_tcp=false
    -Dlisten_unix=true
    -Dmitshm=true
    -Dsecure-rpc=false
    -Dscreensaver=true
    -Dsha1=libcrypto
    -Dxace=true
    -Dxcsecurity=true
    -Dxdm-auth-1=true
    -Dxdmcp=true
    -Dxf86bigfont=false
    -Dxinerama=true
    -Dxres=true
    -Dxselinux=false
    -Dxv=true
    -Dxvfb=true
    -Dxwayland_ei=false
)

MESON_SRC_CONFIGURE_OPTION_SWITCHES=(
    'eglstream xwayland_eglstream'
)

src_install() {
    meson_src_install
    edo rm -r "${IMAGE}"/usr/{share/man/man1/Xserver.1,$(exhost --target)/lib/xorg}
}

