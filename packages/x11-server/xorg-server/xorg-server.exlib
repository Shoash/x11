# Copyright 2007-2008 Alexander Færøy <eroyf@exherbo.org>
# Copyright 2008 Bo Ørsted Andresen <zlin@exherbo.org>
# Copyright 2008 Bernd Steinhauser <berniyh@exherbo.org>
# Copyright 2008-2011 Ingmar Vanhassel <ingmar@exherbo.org>
# Copyright 2014 Friedrich Kröner <friedrich@mailstation.de>
# Distributed under the terms of the GNU General Public License v2
# Based in part upon 'xorg-server-1.4.0.90-r3.ebuild' from Gentoo, which is:
#     Copyright 1999-2008 Gentoo Foundation

require alternatives xorg

export_exlib_phases pkg_setup src_configure src_install

SUMMARY="The X server for X.org"
DOWNLOADS="https://www.x.org/releases/individual/${PN/org-}/${PNV}.tar.xz"

LICENCES="X11"
SLOT="0"

MY_INPUT_DRIVERS=(
    "evdev[>=2.0]" joystick libinput "mouse[>=1.4.0]" synaptics wacom
)
MY_VIDEO_DRIVERS=(
    amdgpu ati cirrus dummy fbdev intel nouveau v4l vesa vmware
)

MYOPTIONS="
    debug
    doc
    kdrive       [[ description = [ An X Server for low memory environments ] ]]
    record       [[ description = [ Build the record extension ] ]]
    suid-wrapper [[ description = [ Install an Xorg wrapper which drops suid capabilities in case they're not required ] requires = [ systemd ] ]]
    systemd      [[ description = [ Use systemd-logind managed fds for input devices and drm nodes ] ]]
    xephyr       [[ description = [ An X Server which targets a window on a host X Server as its framebuffer ] requires = [ kdrive ] ]]
    xwayland     [[ description = [ A compatibility layer for legacy X11 applications on wayland ] ]]
    xwayland-eglstream [[ description = [ xwayland eglstream support (proprietary Nvidia driver) ] requires = [ xwayland ] ]]

    ( providers: libressl openssl ) [[ number-selected = exactly-one ]]
    ( providers: eudev systemd ) [[ number-selected = exactly-one ]]
    ( providers: elogind systemd-logind ) [[
        *description = [ Logind support ]
        number-selected = at-most-one
    ]]
"

DEPENDENCIES="
    build:
        x11-libs/xtrans[>=1.2.2]
        x11-proto/xorgproto[>=2021.4.99.2]
        x11-utils/util-macros[>=1.19.0-r1] [[
            note = [ Introduced a patch for XORG_PROG_RAWCPP to avoid the use of unprefixed cpp ]
        ]]
        doc? (
            app-doc/doxygen[>=1.6.1]
            app-text/xmlto[>=0.0.20]
            x11-doc/xorg-sgml-doctools
        )
        xephyr? ( x11-libs/libXv[>=1.0.5] )
    build+run:
        !x11-drivers/glamor [[ note = [ Glamor moved to $PN ] ]]
        dev-libs/libbsd [[ note = [ libbsd-overlay automagic ] ]]
        dev-libs/libepoxy[>=1.5.4]
        dev-libs/libglvnd [[ note = [ provides libGL ] ]]
        x11-dri/libdrm[>=2.4.89]
        x11-dri/mesa[>=10.2.0] [[ note = [ Needed for glamor/DRI3/xwayland ] ]]
        x11-libs/libpciaccess[>=0.12.901]
        x11-libs/libXau
        x11-libs/libxcvt
        x11-libs/libXdmcp
        x11-libs/libXext[>=1.0.99.4]
        x11-libs/libXfont2[>=2.0.0]
        x11-libs/libXi[>=1.2.99.1]
        x11-libs/libxkbfile
        x11-libs/libxshmfence[>=1.1]
        x11-libs/pixman:1[>=0.27.2]
        x11-libs/xtrans[>=1.3.5]
        !x11-drivers/xf86-video-modesetting [[ note = [ xf86-video-modesetting moved to $PN ] ]]
        providers:elogind? ( sys-auth/elogind )
        providers:eudev? ( sys-apps/eudev )
        providers:libressl? ( dev-libs/libressl:= )
        providers:openssl? ( dev-libs/openssl:= )
        providers:systemd? ( sys-apps/systemd )
        providers:systemd-logind? ( sys-apps/systemd )
        systemd? (
            sys-apps/dbus[>=1.0]
            sys-apps/systemd[>=209]
        )
        xephyr? (
            x11-libs/libxcb[>=1.9.3]
            x11-utils/xcb-util
            x11-utils/xcb-util-image
            x11-utils/xcb-util-keysyms
            x11-utils/xcb-util-renderutil
            x11-utils/xcb-util-wm
        )
    test:
        dev-libs/glib:2[>=1.13]
    run:
        fonts/font-adobe-100dpi
        fonts/font-adobe-75dpi
        fonts/font-alias[>=1.0.1]
        fonts/font-cursor-misc
        fonts/font-misc-misc
        x11-apps/xkbcomp
        x11-apps/xkeyboard-config[>=1.4]
    post:
        xwayland? ( x11-server/xwayland[>=${PV}] )
        xwayland-eglstream? ( x11-server/xwayland[eglstream] )
    suggestion:
        ( $(
            for x in "${MY_INPUT_DRIVERS[@]}"; do
                echo "x11-drivers/xf86-input-${x} [[ description = [ Take with --take xf86-input-${x%%\[*} or --take input-drivers ] ]]"
            done
        ) ) [[ *group-name = [ input-drivers ] ]]
        ( $(
            for x in "${MY_VIDEO_DRIVERS[@]}"; do
                echo "x11-drivers/xf86-video-${x} [[ description = [ Take with --take xf86-video-${x%%\[*} or --take video-drivers ] ]]"
            done
        ) ) [[ *group-name = [ video-drivers ] ]]
"

xorg-server_pkg_setup() {
    # Some bundled modules use xf86LoadSubModule to load another xorg module at runtime which means
    # that those contain undefined symbols which would cause loading these modules to fail when
    # built with -Wl,-z,now
    # TODO(moben): Figure out if there is a way to only apply this to the parts where it's needed,
    #              i.e. build /usr/libexec/Xorg with -Wl,-z,now
    LDFLAGS+=" -Wl,-z,lazy"
}

# unit tests are broken in 1.10
# xfake and xfbdev are deprecated and can be removed once <1.20.0 is gone
xorg-server_src_configure() {
    local myconf=(
        --localstatedir=/var
        --enable-agp
        --enable-composite
        --enable-config-udev
        --enable-config-udev-kms
        --enable-dga
        --enable-dri
        --enable-dri2
        --enable-dri3
        --enable-glamor
        --enable-glx
        --enable-input-thread
        --enable-install-setuid
        --enable-libdrm
        --enable-listen-local
        --enable-listen-unix
        --enable-mitshm
        --enable-present
        --enable-xdmcp
        --enable-xf86-input-inputtest
        --enable-xf86vidmode
        --enable-xinerama
        --enable-xorg
        --enable-xres
        --enable-xshmfence
        --enable-xv
        --enable-xvfb
        --disable-config-hal
        --disable-libunwind
        --disable-listen-tcp
        --disable-windowsdri
        --disable-xnest
        --with-fallback-input-driver=libinput
        --with-fontrootdir=/usr/share/fonts/X11
        --with-sha1=libcrypto
        --without-dtrace
        --without-fop
        $(expecting_tests && echo --enable-unit-tests || echo --disable-unit-tests)
        $(option_enable debug)
        $(option_enable doc docs)
        $(option_enable doc devel-docs)
        $(option_enable kdrive)
        $(option_enable record)
        $(option_enable suid-wrapper)
        $(option_enable xephyr)
        $(option_with doc doxygen)
        $(option_with doc xmlto)
        $(option_with systemd systemd-daemon)
        $(if option providers:elogind || option providers:systemd-logind; then
              echo --enable-systemd-logind
          else
              echo --disable-systemd-logind
          fi)
    )

    econf \
        "${XORG_SERVER_SRC_CONFIGURE_PARAMS[@]}" \
        "${myconf[@]}"
}

xorg-server_src_install() {
    default

    keepdir /etc/X11/
    edo rmdir "${IMAGE}"/var/{log/,}

    edo mv "${IMAGE}"/usr/$(exhost --target)/lib/xorg/modules/extensions/libglx.so{,.xorg}

    option doc && dodoc doc/*.html hw/xfree86/doc/*.html

    # NOTE(somasis): musl hates lazy loading, which Xorg uses a lot of.
    # therefore we install a conf file which loads modules in the correct order
    # so that musl likes it.
    # thanks to Alpine for this fix; http://git.alpinelinux.org/cgit/aports/tree/main/xorg-server/20-modules.conf
    if [[ $(exhost --target) == *-musl* ]];then
        insinto /usr/share/X11/xorg.conf.d
        doins "${FILES}"/20-modules.conf
    fi

    # libglvnd provides the _importance file for eclectic opengl glvnd, xorg-server contributes just
    # one file. Only legacy nvidia drivers supply their own libglx, newer ones load a libglxserver
    # based on the used GPU at runtime
    alternatives_for opengl glvnd '' \
        /usr/$(exhost --target)/lib/xorg/modules/extensions/libglx.so /usr/$(exhost --target)/lib/xorg/modules/extensions/libglx.so.xorg
}

